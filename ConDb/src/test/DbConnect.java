package test;

// SQLを実行するために必要なパッケージを取り込む
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * 商品テーブルに登録された商品名を表示する
 *
 * @author i-learning
 *
 */
public class DbConnect {

    public static void main(String[] args) {
        // 接続情報を定義
        String url = "jdbc:mysql://localhost:3306/hanbai?useUnicode=true&characterEncoding=utf8";
        String user = "root";
        String password = "";

        Connection con = null;

        try {
            // MySQLとの接続を開始
            Class.forName("org.gjt.mm.mysql.Driver");
            con = DriverManager.getConnection(url, user, password);
            Statement startment = con.createStatement();
            System.out.println("接続成功");

            // 実行するSQLを定義
            String sql = "SELECT * FROM tshohin";

            // 定義したSQLを実行して、実行結果を変数resultに代入
            ResultSet result = startment.executeQuery(sql);

            // 変数resultの内容を順次出力
            while (result.next()) {
                System.out.println(result.getString("shohinmei"));
            }

        // エラー情報を取得
        } catch (Exception e) {
            System.out.println("例外発生:" + e);
        } finally {
        	try{
            	// MySQLとの接続を終了
                con.close();
                System.out.println("接続終了");
        	} catch (Exception e){
                System.out.println("例外発生:" + e);
        	}
        }
    }

}
